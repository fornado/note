#### Dependency patterns

1. Dependency Injection

   >  Provide all dependencies outside the object-under-construction

2. Service Locator

   > A *Service Locator* is an object that can create dependencies and hold onto dependencies.

3. Environment

   > an *environment* is a mutable struct that provides all the dependencies needed by objects-under-construction.

   It is accessed inside objects-under-construction, while *service Locator* is provided to the ouc.

4. Protocol Extension

   > it uses Swift protocol extensions to allow the ouc to get access to its dependencies.

#### Dependency Injection

1. Goal is to provide dependencies to the ouc from **outside**.

2. Types of injection

   1. *Initializer* : the consumer provides dependencies to ouc initializer;

   2. Property : setting a stored-property on the ouc.

   3. Method: the consumer provides dependencies when calling a method on ouc.

      > The less state an object has, the better. The shorter an object's lifetime, the better.
      >
      > A good rule of **thumb**: when cannot function without a dependency, user *initializer injection*, while can function without a dependency, you can use any type of injection, preferably initializer injection.

3. Circular dependency

   use *property* or *method* injection in one of the two objects.

4. Substituting dependency implementations

   When designing an ouc, user **protocol** types for dependencies.

   1. Compile-time substitution
      1. add *compilation condition identifiers* to active compilation conditions build setting.
   2. Runtime substitution
      1. Write an if statement around the dependency instantiation, a remote-feature flag service, or local values and so on.

#### Dependency Injection approaches

> **ephemeral dependencies** is dependencies that can be instantiated and destoried at the same time as the ouc, while **long-lived dependencies** is lifetime longer than ouc.

1. On-demand

   > the consumer is responsible for gathering all dependencies and is responsible for providing those dependencies to the ouc via the initializer, a stored-property or a method

   1. Initializing ephemeral dependencies

      the *ephemeral* dependencies is created and destroyed alongside the ouc.

      Consumer can initializing all the dependencies.

   2. Finding long-lived dependencies

      Consumer should find a reference to the dependency and pass to ouc.

   3. Substituting dependency implementations

      use **On-demand** approach, consumer should find all the places a dependency is instantiated and wrap the instantiation with a compilation condition or a runtime conditional statement.

   4. Pros of the on-demand approach

      1. Pros list
         1. Easy to explain and easy to understand.
         2. code is testable
         3. Defer decisions
         4. Work with other teammate at same time;
      2. Cons list
         1. Dependency instantiations are decentralized, initialization logic can be duplicated many times.
         2. Consumer might duplicate a lot of dependencies of instantiate loggic

2. Factories

   about centralizing dependency instantiation approach.

   it works for **ephemeral dependencies**,it does not address managing **long-lived dependencies** such as singletons.

   1. Dependency factory methods

      1. Creating and getting transitive dependencies

   2. Object-under-construction factory methods

   3. Getting runtime values

   4. Substituting dependency implementations

   5. Injecting factories

      to create multiple instances of dependencies from the outside, we use

      1. Using closures

      2. Using protocols

         Declare a factory protocol to delegate the creation of a dependency to factories class.

   6. Creating a factories object

   7. Pros of the factories approach

      1. ephemeral dependencies are created in a central place 
      2. Substituting a large amount of dependencies is much easier.
      3. consumers are more resilient to change
      4. code is easier to read

   8. Cons of the factories approach

      1. need to break up large factories classes into multiple classes
      2. Only works for ephemeral object.

3. Single container

   a **container** is like a factories class that can hold onto *long-lived* dependencies. It is a stateful version of a factories class.

   

4. Container hierarchy

5. 

1. 