---
Aadadfaaaa 
---

# Typora 笔记

> [toc] 
>
> 

## 概述

## 类型

### 块元素

* 标头 使用 `#` 在行的开头使用1-6个hash()字符，对应标头级别1-6
* 块引用 使用 `>` 字符进行块引用
* 清单 输入 `*` 创建无序列表， 输入 `1. ` 创建有序列表
* 任务列表 输入  `- [ ]` 标记为没完成的项目列表
* 代码块 输入 ` ``` ` 并按return

```swift
if let name = name {
  dafa
}
```

* 数学块 输入 `$$` 并按Retrun

* Table 输入 ` | First Header | Second Header |` 并return

  * | First header | Second Header |
    | -----------: | ------------- |
    |         Aaaa | Bbb           |

* 脚注 使用 `[^footnote]` 

  Here is the *text* of the **footnote**.

* 水平尺 在空白行上输入 `***` 或者 `---` 

  ***

* YAML首要事项 在文档顶部输入 `---`

* 目录（TOC） 输入 `[toc]` 

  > [toc] 

* 

### 跨度元素

* 链接 输入 `[description](http://baidu.com/ "Title")`

* 内部连接  将 `href` 设置为 `headers`

* 参考链接 使用第二组方括号 `[an example][id]`

  * This is [an example][id] reference-style link.

    > This is [Baidu][] reference-style link.
    >
    > Then, anywhere in the document, you define your link lable on a line by itself like this:
    >
    > [Baidu]: http://www.baidu.com/ "Optional Title Here"

  

  * 网址 使用 `<>` 括起来

    * > <i@typora.io>

  * 图片 使用 ![Alt text](/path/to/img.jpg "Optional title")

    * > This is an Image:   ![屏幕快照 2019-09-30 下午3.38.32](/Users/lb/Desktop/屏幕快照 2019-09-30 下午3.38.32.png)

  

  * 重点 使用 `*` 和 `_` 包裹

    * > *single asterisks*
      >
      > _single underscores_
      >
      > \*this text is surround by literal asterisks\* 

  * 强大 使用double * 或_ 会导致其包含的内容被HTMl<strong>包裹

    * > **double asterisks** 
      >
      > __double underscores__

  * Code 使用 ``

    * > Use the `printf()` function.

  * 删除线 使用 ` ~~` 

    * ~~Mistaken text.~~ 

  * 下划线 使用 `<u>title</u>` 

    *  *<u>title</u>*

  * 表情符号 使用 `happy:` 

    * > :smile:

  * 内联数学 使用 `$ Tex $` 

  * 下标 去Preference面板->Markdown选项中启用，然后使用 `~` 包装

  * 上标 同下标启用后，使用 `^` 

  * 高亮 同上启用后，使用 ` == ` 包裹

    * > ==hightlight== 

### HTML

* ###  HTML 可以使用HTML来设置Markdown不支持的内容样式，如 ><span style="color:red">this text is red</span>

  * 嵌入内容
* 嵌入视频
  
  * 其他
